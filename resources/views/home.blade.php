@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ Auth::user()->id }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                <div class="card-body">
                    <a href="/home/create" class="btn btn-primary">Input Province</a>
                    <br/>
                    <br/>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Penginput</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($province as $p)
                            <tr>
                                <td>{{ $p->provice_id }}</td>
                                <td>{{ $p->provice_code }}</td>
                                <td>{{ $p->provice_name }}</td>
                                <td>{{ $p->name }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
