@extends('layouts.app')

@section('content')

<div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    <strong>Province</strong>
                </div>
                <div class="card-body">

                    <form method="POST" action="/home/store">

                        {{ csrf_field() }}  

                        <div class="form-group">
                            <label>Code</label>
                            <input type="text" name="provice_code" class="form-control" placeholder="Province Code">

                            @if($errors->has('provice_code'))
                                <div class="text-danger">
                                    {{ $errors->first('provice_code')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Province Name</label>
                            <textarea name="provice_name" class="form-control" placeholder="Province Name .."></textarea>
                             @if($errors->has('provice_name'))
                                <div class="text-danger">
                                    {{ $errors->first('provice_name')}}
                                </div>
                            @endif

                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>

                </div>
            </div>
        </div>
@endsection
