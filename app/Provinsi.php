<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    //
    protected $table = "province";


    protected $fillable = ['provice_code', 'provice_name', 'created_by','updated_by', 'deleted_by'];
}
